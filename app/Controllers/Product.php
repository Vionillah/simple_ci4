<?php
namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\ProductModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Product extends Controller
{
    public function __construct()
    {
        helper('form');
        $this->product = new ProductModel();
    }

    public function index()
    {
        $data['validation'] = $this->validator;
        $data['product'] = $this->product->getProduct();
        echo view('product/index', $data);

    }

    public function store()
    {
        $validated = $this->validate([
            'product_image' => 'uploaded[product_image]|mime_in[product_image,image/jpg,image/jpeg,image/gif,image/png]|max_size[product_image,4096]'
        ]);
  
        if ($validated == FALSE) {
            session()->setFlashdata('danger', 'Choose A Valid File');
            return redirect()->to(base_url('product'));
        } else {
 
            $name = $this->request->getPost('product_name');
            $desc = $this->request->getPost('product_description');
            $file = $this->request->getFile('product_image');
            $file->move(ROOTPATH . 'public\uploads');
            $filename = $file->getName(); 

            $data = [
                'product_name' => $name,
                'product_description' => $desc,
                'product_image' => $filename
            ];

            $save = $this->product->insert_product($data);
        
            if($save)
            {
                session()->setFlashdata('success', 'Created product successfully');
                return redirect()->to(base_url('product')); 
            }
        }
    }

    public function edit($id)
    {
        $data['product'] = $this->product->getProduct($id);
        return view('product/edit', $data);
    }

    public function update()
    {
        $id = $this->request->getPost('product_id');
        $validated = $this->validate([
            'product_image' => 'uploaded[product_image]|mime_in[product_image,image/jpg,image/jpeg,image/gif,image/png]|max_size[product_image,4096]'
        ]);

        if ($this->request->getFile('product_image') == "") {
            $data = [
                'product_name' => $this->request->getPost('product_name'),
                'product_description' => $this->request->getPost('product_description'),
            ];
            $update = $this->product->update_product($data, $id);
            if($update)
            {
                session()->setFlashdata('info', 'Updated product without image successfully');
                return redirect()->to(base_url('product')); 
            }
        } else if ($this->request->getFile('product_image')) {
            if ($validated == FALSE) {
                session()->setFlashdata('danger', 'Updated product image failed, Please Choose a Valid Image');
                return redirect()->to(base_url('product')); 
            } else {
                $name = $this->request->getPost('product_name');
                $desc = $this->request->getPost('product_description');
                $file = $this->request->getFile('product_image');
                if ($this->request->getPost('old_image')){
                    unlink('uploads/' . $this->request->getPost('oldImage'));
                }
                $file->move(ROOTPATH . 'public\uploads');
                // delete old image
                $filename = $file->getName(); 

                $data = [
                    'product_name' => $name,
                    'product_description' => $desc,
                    'product_image' => $filename
                ];
                $update = $this->product->update_product($data, $id);
                if($update)
                {
                    session()->setFlashdata('info', 'Updated product successfully');
                    return redirect()->to(base_url('product')); 
                }
            }
        }
    }

    public function delete($id)
    {
        $post = $this->product->getProduct($id);

        // delete image
        if ($post['product_image'] == true) {
            unlink('uploads/' . $post['product_image']);
        }

        $delete = $this->product->delete_product($id);
        if($delete)
        {
            session()->setFlashdata('warning', 'Deleted product successfully');
            return redirect()->to(base_url('product'));
        }
    }

    public function exportExcel()
    {
        // // ambil data transaction dari database
        $product = $this->product->getProduct();
        // // panggil class Sreadsheet baru
        $spreadsheet = new Spreadsheet;

        $spreadsheet->getActiveSheet()
                    ->mergeCells('A1:D1')
                    ->mergeCells('A2:D2');
        $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'LAPORAN PRODUK PENJUALAN')
                    ->setCellValue('A2', '')
                    ->setCellValue('A3', 'No')
                    ->setCellValue('B3', 'Nama Produk')
                    ->setCellValue('C3', 'Gambar')
                    ->setCellValue('D3', 'Deskripsi');
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(500);
        $spreadsheet->getActiveSheet()->getStyle('A1:A10')->getAlignment()->setHorizontal(Alignment::VERTICAL_CENTER)->setVertical(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B1:B10')->getAlignment()->setHorizontal(Alignment::VERTICAL_CENTER)->setVertical(Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('D1:D10')->getAlignment()->setHorizontal(Alignment::VERTICAL_CENTER)->setVertical(Alignment::HORIZONTAL_CENTER);

        // // define kolom dan nomor
        $kolom = 4;
        $nomor = 1;
        // // tambahkan data transaction ke dalam file excel
        foreach($product as $data) {
            $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue('A' . $kolom, $nomor)
                        ->setCellValue('B' . $kolom, $data['product_name'])
                        ->setCellValue('D' . $kolom, $data['product_description']);

            $spreadsheet->getActiveSheet()->getRowDimension($kolom)->setRowHeight(100);
            if ($data['product_image']) {
                $objDrawing = new Drawing();
                $objDrawing->setPath('uploads/'. $data['product_image']);
                $objDrawing->setHeight(100);
                $objDrawing->setCoordinates('C'.$kolom);
                $objDrawing->setWorksheet($spreadsheet->getActiveSheet());
            }
            $kolom++;
            $nomor++;
        }
        // download spreadsheet dalam bentuk excel .xlsx
        $writer = new Xlsx($spreadsheet);
    
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan_produk.xlsx"');
        header('Cache-Control: max-age=0');
    
        $writer->save('php://output');
    }
}
?>