<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Libraries\Notification;
use App\Models\ProductModel;

class Admin extends Controller
{
    protected $helpers = ['form','date'];
    protected $session = null;
    protected $request = null;

	public function __construct()
	{
        $this->session = session();
        $this->request = \Config\Services::request();
        $this->notif = new Notification();
        $this->moduser = model('App\Models\UserModel');
        $this->product = new ProductModel();

	}

	public function index()
	{
        $data['product'] = $this->product->getProduct();
   
        if (isset($_SESSION['admin_logged_in'])) {
            return redirect()->to(base_url('product'));
        }
        return view('admin/login');
	}

    public function login()
    {
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');

        if (isset($_SESSION['admin_logged_in'])) {
            return redirect()->to(base_url('admin'));
        }
        if (!isset($_SESSION['admin_logged_in']) && isset($username) && isset($password)) {
            $datauser = [
                'username' => $username,
                'password' => md5($password)
            ];
            $user = $this->moduser->asObject()->where($datauser)->limit(1)->find();
            if (count($user) > 0) {
                $data_session = array(
                    'admin_user_id' => $user[0]->user_id,
                    'admin_username' => $user[0]->username,
                    'admin_logged_in' => TRUE
                );
                $this->session->set($data_session);
                return redirect()->to(base_url('admin'));
            } 
            
			$users = $this->moduser->where('username', $username)->first();

			if($users)
			{
				$salt = $users->salt;
				if($users->password!==md5($salt.$password))
				{
                    session()->setFlashdata('danger', 'Password Salah !');
                    return redirect()->to(base_url('admin'));
				}
			}else{
                session()->setFlashdata('danger', 'Data Tidak Ditemukan');
                return redirect()->to(base_url('admin'));
			}
        }
    }

    public function logout(){
        $array_items = array('admin_user_id','admin_username','admin_logged_in');
        $this->session->remove($array_items);
        return redirect()->to(base_url('admin'));
    }
}