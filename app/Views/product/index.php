<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD CI 4</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
</head>
<body>

    <div class="container mt-5 mb-5 text-center">
        <h4>CRUD Codeigniter 4</h4>
    </div>
    <div class="container">
        <?php
        if(!empty(session()->getFlashdata('success'))){ ?>
 
        <div class="alert alert-success">
            <?php echo session()->getFlashdata('success');?>
        </div>
             
        <?php } ?>
        <?php if(!empty(session()->getFlashdata('info'))){ ?>
 
        <div class="alert alert-info">
            <?php echo session()->getFlashdata('info');?>
        </div>
             
        <?php } ?>
        <?php if(!empty(session()->getFlashdata('danger'))){ ?>

        <div class="alert alert-danger">
            <?php echo session()->getFlashdata('danger');?>
        </div>
             
        <?php } ?>
 
        <?php if(!empty(session()->getFlashdata('warning'))){ ?>
 
        <div class="alert alert-warning">
            <?php echo session()->getFlashdata('warning');?>
        </div>
             
        <?php } ?>
    </div>
    <div class="container">
        <button class="btn btn-primary float-left mb-3 mr-3" data-toggle="modal" data-target="#addModal"><i class="fas fa-plus mr-3"></i>Tambah Produk</button>
        <a class="btn btn-success float-left mb-3" href="<?= base_url('product/exportExcel'); ?>">Export Excel</a>
        <a href="<?= base_url('admin/logout');?>" class="btn btn-danger float-right mb-3"><i class="fas fa-sign-out-alt mr-3"></i>Logout</a>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th>No</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    <?php 
                    foreach($product as $key => $data) { ?>
                    <tr>
                        <td><?php echo $key+1; ?></td>
                        <td><?php echo $data['product_name']; ?></td>
                        <td class="text-center">
                            <img src="<?= base_url()?>/uploads/<?= $data['product_image'];?>" alt="Image"
                            style="max-height: 200px; max-width:200px;">
                        </td>
                        <td><?php echo $data['product_description']; ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="#" class="btn btn-info btn-sm btn-edit" data-id="<?= $data['id'];?>" data-name="<?= $data['product_name'];?>" data-image="<?= $data['product_image'];?>" data-desc="<?= $data['product_description'];?>"><i class="fas fa-edit"></i></a>
                                <a href="<?= base_url('product/delete/'.$data['id']); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus produk <?php echo $data['product_name']; ?> ini?')"><i class="fas fa-trash-alt"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <!-- Modal Add Product-->
    <form action="<?= base_url('product/store/'); ?>" method="post" enctype="multipart/form-data">
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add New Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Product Name</label>
                            <input type="text" class="form-control" name="product_name" placeholder="Nama Produk">
                        </div>
                        <div class="form-group">
                            <label>Product Image</label>
                            <br>
                            <input type="file" name="product_image" id="product_image">
                        </div>
                        <div class="form-group">
                            <label>Product Description</label>
                            <input type="text" class="form-control" name="product_description" placeholder="Deskripsi Produk">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Add Product-->
    
    
    <!-- Modal Edit Product-->
    <form action="<?= base_url('product/update/'); ?>" method="post" enctype="multipart/form-data">
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add New Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Product Name</label>
                            <input type="text" class="form-control product_name" name="product_name" placeholder="Nama Produk">
                            <input type="hidden" class="form-control product_image" name="oldImage">
                        </div>
                        <div class="form-group">
                            <label>Product Image</label>
                            <br>
                            <input type="file" name="product_image" id="product_image">
                        </div>
                        <div class="form-group">
                            <label>Product Description</label>
                            <input type="text" class="form-control product_description" name="product_description" placeholder="Deskripsi Produk">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="product_id" class="product_id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- End Modal Edit Product-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    
    <script>
        $(document).ready(function(){
            $('.btn-edit').on('click',function(){
                const id = $(this).data('id');
                const name = $(this).data('name');
                const image = $(this).data('image');
                const desc = $(this).data('desc');
                $('.product_id').val(id);
                $('.product_name').val(name);
                $('.product_image').val(image);
                $('.product_description').val(desc);
                $('#editModal').modal('show');
            });
        });
    </script>
</body>
</html>